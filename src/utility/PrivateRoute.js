import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ component: Component, isAuthenticated, isAdmin, ...rest }) => (
	<Route {...rest} render={props => (
		(isAuthenticated && isAdmin)
			? (
				<Component {...props}>
					{props.children}
				</Component>
			)
			: (
				<Redirect to={{
					pathname: '/home',
					state: { from: props.location }
				}} />
			)
	)} />
);

export default PrivateRoute;