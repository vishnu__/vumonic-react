const STRING = {
    truncate: (value, len) => {
        if (typeof (value) === 'string' && typeof (len) === 'number') {
            if (value.length <= len) {
                return value;
            } else {
                return value.substr(0, len - 3) + "...";
            }
        } else {
            return "";
        }
    },
    validateEmail: (email) => {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
}

export { STRING }