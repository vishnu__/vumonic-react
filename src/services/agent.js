import axios from 'axios';
import store  from '../redux/store';

const develop = true;

const CLOUD = {
	LOCAL : {
		API_ROOT : 'http://localhost:8081/',
	},
	SERVER : {
		API_ROOT : ''
	}
}

let URL = {};
if( develop ){
	URL = Object.assign({}, CLOUD.LOCAL )
}else{
	URL = Object.assign({}, CLOUD.SERVER )
}


const request = {
	get: async (url, token) => {
		try {
			const res = await axios(URL.API_ROOT + url, {
				method: 'get',
				token,
				validateStatus: _status => {
					return _status >= 200 && _status < 500;
				}
			});
			return res;
		}
		catch (error) {
			return error;
		}
	},
	post: async (url, body, token) => {
		try {
			const res = await axios(URL.API_ROOT + url, {
				method: 'post',
				data: body,
				token,
				validateStatus: _status => {
					return _status >= 200 && _status < 500;
				}
			});
			return res;
		}
		catch (error) {
			return error;
		}
	}
}

const Auth = {

	signup : payload => {
		return request.post('user/signup/', payload);
	},

	login : payload => {
		return request.post('user/login/', payload);
	},

	logout : payload => {
		return request.post('user/revoke/',payload);
	},

	refresh : async (url, payload, token) => {
		try {
			const res = await axios.post(URL.API_ROOT + url, payload, token);
			return res;
		}
		catch (error) {
			console.log(error);
		}
	},
}

const Blog = {

	listArticle : payload => {
		let token = store.getState().auth.token;
		if(token !== '' && token !== undefined)
			axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
		return request.get('blog/list-article/', payload);
	},

	getArticle : id => {
		return request.get(`blog/list-article/?id=${id}`);
	},

	listTopics : payload => {
		let token = store.getState().auth.token;
		axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
		return request.get('blog/topic/', payload);
	},

	createArticle: payload => {
		let token = store.getState().auth.token;
		axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
		axios.defaults.headers.common['Content-Type'] = 'multipart/form-data';
		return request.post('blog/article/', payload);
	},

	createTopic : payload => {
		let token = store.getState().auth.token;
		axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
		axios.defaults.headers.common['Content-Type'] = 'multipart/form-data';
		return request.post('blog/topic/', payload);
	}
}

export default {
	Auth,
	Blog
}