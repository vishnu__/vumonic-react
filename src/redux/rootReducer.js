import { combineReducers } from 'redux';
import { createBrowserHistory } from 'history';

import authReducer from './reducers/authReducer';
import commonReducer from './reducers/commonReducer';

const history = createBrowserHistory();

export default combineReducers({
    auth: authReducer,
    common: commonReducer,
    router: history,
});