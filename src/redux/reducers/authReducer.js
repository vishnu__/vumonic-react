import {auth as initialState} from '../initialState';
import { 
    LOGOUT,
    UPDATE_TOKEN,
    UPDATE_USER_DATA,
    UPDATE_ISAUTHENTICATED, 
} from '../actions/actionsTypes';

export default (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_ISAUTHENTICATED: {
            return {
                ...state,
                isAuthenticated: action.value,
            }
        }

        case UPDATE_TOKEN: {
            return {
                ...state,
                token: action.value,
            }
        }

        case UPDATE_USER_DATA: {
            return {
                ...state,
                userInfo: {...action.value}
            }
        }

        case LOGOUT: {
            return { 
                ...state,
                userInfo: {},
                isAuthenticated: false,
                token: ''
            }
        }

        default: return state
    }
}