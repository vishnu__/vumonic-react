import { common as initialState } from '../initialState';
import {
    TOGGLE_LOADER,
} from '../actions/actionsTypes';

export default (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_LOADER: {
            return {
                ...state,
                loader: action.value,
            }
        }

        default: return state
    }
}