import { createStore } from 'redux';
import { createBrowserHistory } from 'history';

import rootReducer from './rootReducer';
import { auth, common } from './initialState';

export const history = createBrowserHistory();

const initialState = {
    auth,
    common
}

const store = createStore(
    rootReducer,
    initialState,
)

export default store;