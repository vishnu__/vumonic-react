export const auth = {
    isAuthenticated: false,
    token: '',
    userInfo: {},
}

export const common = {
    loader: false,
}