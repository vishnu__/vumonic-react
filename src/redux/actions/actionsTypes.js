//actions for auth reducer
export const UPDATE_USER_DATA = 'UPDATE_USER_DATA';
export const UPDATE_TOKEN = 'UPDATE_REFRESH_TOKEN';
export const UPDATE_ISAUTHENTICATED = 'UPDATE_ISAUTHENTICATED';
export const LOGOUT = 'LOGOUT';

export const TOGGLE_LOADER = 'TOGGLE_LOADER';