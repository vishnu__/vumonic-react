import React, { Component } from 'react';
import { Editor, EditorState, RichUtils, AtomicBlockUtils, Entity, convertToRaw } from 'draft-js';
import Icon from 'react-icons-kit';
import { bold } from 'react-icons-kit/fa/bold';
import { italic } from 'react-icons-kit/fa/italic';
import { underline } from 'react-icons-kit/fa/underline';
import { ic_format_list_bulleted } from 'react-icons-kit/md/ic_format_list_bulleted';
import { ic_format_list_numbered } from 'react-icons-kit/md/ic_format_list_numbered';
import { ic_code } from 'react-icons-kit/md/ic_code';
import { ic_format_quote } from 'react-icons-kit/md/ic_format_quote';
import { image } from 'react-icons-kit/fa/image';
import { link } from 'react-icons-kit/fa/link';


import './RichTextEditor.css';

const mediaBlockRenderer = (block) => {
    if (block.getType() === 'atomic') {
        return {
            component: Media,
            editable: false
        };
    }
    return null;
}

const Image = (props) => {
    return <img src={props.src} />;
};

const Media = (props) => {

    const entity = Entity.get(props.block.getEntityAt(0));

    const { src } = entity.getData();
    const type = entity.getType();

    let media;
    if (type === 'image') {
        media = <Image style={{ maxWidth: '200px' }} src={src} />;
    }

    return media;
};

const StyleButton = (props) => {

    const onToggle = (e) => {
        e.preventDefault();
        props.onToggle(props.style);
    };


    let className = 'RichEditor-styleButton';
    if (props.active) {
        className += ' RichEditor-activeButton';
    }

    return (
        <span className={className} onMouseDown={onToggle}>
            {props.label}
        </span>
    );

}

const BLOCK_TYPES = [
    { label: 'H2', style: 'header-two' },
    { label: 'H3', style: 'header-three' },
    { label: <Icon icon={ic_format_quote} size={18} />, style: 'blockquote' },
    { label: <Icon icon={ic_format_list_bulleted} size={18} />, style: 'unordered-list-item' },
    { label: <Icon icon={ic_format_list_numbered} size={18} />, style: 'ordered-list-item' },
    { label: <Icon icon={ic_code} size={18} />, style: 'code-block' },
];

const BlockStyleControls = (props) => {
    const { editorState } = props;
    const selection = editorState.getSelection();
    const blockType = editorState
        .getCurrentContent()
        .getBlockForKey(selection.getStartKey())
        .getType();

    return (
        <div className="RichEditor-controls">
            {BLOCK_TYPES.map((type) =>
                <StyleButton
                    key={type.label}
                    active={type.style === blockType}
                    label={type.label}
                    onToggle={props.onToggle}
                    style={type.style}
                />
            )}
        </div>
    );
};

var INLINE_STYLES = [
    { label: <Icon icon={bold} size={16} />, style: 'BOLD' },
    { label: <Icon icon={italic} size={16} />, style: 'ITALIC' },
    { label: <Icon icon={underline} size={16} />, style: 'UNDERLINE' },
];

const InlineStyleControls = (props) => {
    var currentStyle = props.editorState.getCurrentInlineStyle();
    return (
        <div className="RichEditor-controls">
            {INLINE_STYLES.map(type =>
                <StyleButton
                    key={type.label}
                    active={currentStyle.has(type.style)}
                    label={type.label}
                    onToggle={props.onToggle}
                    style={type.style}
                />
            )}
        </div>
    );
};

class RichTextEditor extends Component {

    constructor(props) {
        super(props)
        this.state = {
            editorState: this.props.editorState,
            showURLInput: false,
            urlValue: '',
            image: this.props.image || false,
        }

    }

    toggleBlockType = (blockType) => {
        this.onChange(
            RichUtils.toggleBlockType(
                this.state.editorState,
                blockType
            )
        );
    }

    toggleInlineStyle = (inlineStyle) => {
        this.onChange(
            RichUtils.toggleInlineStyle(
                this.state.editorState,
                inlineStyle
            )
        );
    }

    handleKeyCommand = (command, editorState) => {
        const newState = RichUtils.handleKeyCommand(editorState, command);
        if (newState) {
            this.onChange(newState);
            return 'handled';
        }
        return 'not-handled';
    }

    onTab = (e) => {
        const maxDepth = 4;
        this.onChange(RichUtils.onTab(e, this.state.editorState, maxDepth));
    }

    onChange = editorState => {
        this.setState({
            editorState: editorState
        }, () => {
            this.props.updateState(editorState)
        });
    }

    uploadImage = event => {
        let reader = new FileReader();
        if (event.target.files[0]) {
            reader.onloadend = () => {
                const newEditorState = this.insertImage(this.state.editorState, reader.result);
                this.setState({ editorState: newEditorState });
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    }

    insertImage = (editorState, base64) => {
        const contentState = editorState.getCurrentContent();
        const contentStateWithEntity = contentState.createEntity(
            'image',
            'IMMUTABLE',
            { src: base64 },
        );
        const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
        const newEditorState = EditorState.set(
            editorState,
            { currentContent: contentStateWithEntity },
        );
        return AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey, ' ');
    };

    componentDidUpdate(prevProps) {
        if(this.props.editorState !== prevProps.editorState) {
            this.setState({
                editorState: this.props.editorState
            })
        }
    }

    render() {
        return (
            <div className='rich-text-editor-container'>

                <div className='rich-text-controls-area'>
                    <BlockStyleControls
                        editorState={this.state.editorState}
                        onToggle={this.toggleBlockType}
                    />
                    <InlineStyleControls
                        editorState={this.state.editorState}
                        onToggle={this.toggleInlineStyle}
                    />
                    <Icon
                        className={this.state.image ? 'add-image-icon' : 'hide'}
                        icon={image}
                        onClick={() => this.refs.uploadImage.click()}
                        size={18}
                    />
                    <Icon
                        className='add-image-icon'
                        icon={link}
                        onClick={this.openUrlModal}
                        size={18}
                    />

                    <input
                        className='no-display'
                        type="file"
                        accept="image/*"
                        id="coverPicture"
                        ref="uploadImage"
                        onChange={this.uploadImage}
                    />
                </div>
                <Editor
                    editorState={this.state.editorState}
                    blockRendererFn={mediaBlockRenderer}
                    handleKeyCommand={this.handleKeyCommand}
                    onChange={this.onChange}
                    onTab={this.onTab}
                    ref="editor"
                />
            </div>
        )
    }

}

export default RichTextEditor;

export { InlineStyleControls, BlockStyleControls }