import React, { Component } from 'react';
import { BoxLoading } from 'react-loadingg';
import { connect } from 'react-redux';
import { Switch, Route, withRouter, Redirect, Link } from "react-router-dom";
import { bindActionCreators } from 'redux';
import { Image } from 'react-bootstrap';
import { Button } from 'reactstrap';
import { ToastContainer, toast, Slide } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { UPDATE_USER_DATA, UPDATE_ISAUTHENTICATED, UPDATE_TOKEN, LOGOUT } from '../../redux/actions/actionsTypes';
import PrivateRoute from '../../utility/PrivateRoute';
import Login from '../Login/Login';
import SignUp from '../SignUp/SignUp';
import Home from '../Home/Home';
import Article from '../Article/Article';
import Admin from '../Admin/Admin';
import './App.css';

const mapStateToProps = state => {
    return {
        loader: state.common.loader,
        isAuthenticated: state.auth.isAuthenticated,
        isAdmin: state.auth.userInfo.isAdmin,
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    updateUser: value => dispatch({ type: UPDATE_USER_DATA, value }),
    updateToken: value => dispatch({ type: UPDATE_TOKEN, value }),
    onLogout: value => dispatch({ type: LOGOUT }),
    updateAuthentication: value => dispatch({ type: UPDATE_ISAUTHENTICATED, value }),
}, dispatch)

class App extends Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const user = JSON.parse(localStorage.getItem('user_info')) || undefined;
        const token = localStorage.getItem('token') || undefined;
        if (user !== undefined) {
            this.props.updateUser(user);
            this.props.updateAuthentication(true);
            this.props.updateToken(token);
        }
    }

    logout = () => {
        localStorage.clear();
        this.props.history.go(0);
        this.props.onLogout()
    }

    render() {
        return (
            <div className="App full-space">
                <ToastContainer
                    position={toast.POSITION.TOP_CENTER}
                    closeOnClick={true}
                    hideProgressBar={true}
                    pauseOnFocusLoss={false}
                    transition={Slide}
                    duration={3000}
                />
                <nav className='nav-bar'>
                    <div className='flex-row-start'>
                        <Image src='/logo.png' className='nav-logo' onClick={() => this.props.history.push('/home')} />
                    </div>
                    {this.props.isAuthenticated
                        ? <div className='user-action-button'>
                            {this.props.isAdmin
                                ?
                                <Button className='action-button' onClick={() => this.props.history.push('/admin/home')} >Admin</Button>
                                : ''}
                            <Button className='action-button' onClick={this.logout}>Logout</Button>
                        </div>
                        : <div className='user-action-button'>
                            <Button className='action-button' onClick={() => this.props.history.push('/sign-in')} >Sign In</Button>
                            <Button className='action-button' onClick={() => this.props.history.push('/sign-up')} >Sign Up</Button>
                        </div>
                    }
                </nav>
                <div className='main-container'>
                    <Switch>
                        <Route exact path="/" render={() => <Redirect to="/home" />} />
                        <Route exact path='/sign-in' component={Login} />
                        <Route exact path='/sign-up' component={SignUp} />
                        <Route exact path='/home' component={Home} />
                        <Route exact path='/article/:id' component={Article} />
                        <PrivateRoute
                            path='/admin'
                            isAuthenticated={this.props.isAuthenticated}
                            isAdmin={this.props.isAdmin}
                            component={Admin}
                        />
                    </Switch>
                </div>
                <div className={this.props.loader ? 'loader-main full-space' : 'no-display'}>
                    <BoxLoading speed={.8} size='large' color='var(--secondary-light)' />
                </div>
            </div>
        );
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
