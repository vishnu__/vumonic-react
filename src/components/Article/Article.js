import React, { Component } from 'react';
import { Image } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import Icons from 'react-icons-kit';
import { fileTextO } from 'react-icons-kit/fa/fileTextO';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button } from 'reactstrap';
import { Editor, EditorState, convertFromRaw, Entity } from 'draft-js';

import { TOGGLE_LOADER } from '../../redux/actions/actionsTypes';
import agent from '../../services/agent';
import './Article.css';

const mapDispatchToProps = dispatch => bindActionCreators({
    toggleLoader: value => dispatch({ type: TOGGLE_LOADER, value }),
}, dispatch)

class Article extends Component {
    constructor(props) {
        super(props);
        this.state = {
            article: {},
            similar: [],
        }
    }

    getArticle = () => {
        this.props.toggleLoader(true);
        agent.Blog.getArticle(this.props.match.params.id)
            .then(res => {
                this.props.toggleLoader(false)
                try {
                    if (res.status === 200) {
                        this.setState({
                            article: res.data.result,
                            similar: res.data.similar,
                        })
                    } else {
                        this.setState({
                            article: {},
                        })
                    }

                } catch (e) {
                    this.setState({
                        article: {}
                    })
                }
            })
    }

    componentDidMount() {
        this.getArticle();
    }

    componentDidUpdate(prevProps) {
        if(this.props.match.params.id !== prevProps.match.params.id) {
            this.getArticle();
        }
    }

    mediaBlockRenderer = (block) => {
        if (block.getType() === 'atomic') {
            return {
                component: this.Media,
                editable: false
            };
        }
        return null;
    }

    Media = (props) => {
        const entity = Entity.get(props.block.getEntityAt(0));
        const { src } = entity.getData();
        const type = entity.getType();
        let media;

        if (type === 'image') {
            media = <Image style={{ maxWidth: '200px' }} src={src} />;
        }
        return media;
    };

    render() {
        let content = EditorState.createEmpty();
        if (Object.keys(this.state.article).length) {
            content = EditorState.createWithContent(convertFromRaw(JSON.parse(this.state.article.content)));
        }
        return (
            <div className="article-display-main">
                <div className='back-button-area'>
                    <Button className='article-back-button' onClick={() => this.props.history.push('/home')}>
                        {'<'} Back
                    </Button>
                    <div style={{ marginLeft: '25px', display: 'flex', flexWrap: 'nowrap' }}>
                        {this.state.article.tags !== undefined
                            ? this.state.article.tags.map(tag => (<span className='tag'>{tag}</span>))
                            : ''}
                    </div>
                </div>
                <div className='article-main-container'>
                    <div className='article-display-container'>
                        <div className='image-banner-area'>
                            <Image
                                className='image-banner'
                                src={this.state.article.image === '' ? '/assets/article-banner.png'
                                    : 'data:image/jpeg;base64,' + this.state.article.image}
                            />
                            <div className='title-area'>
                                {this.state.article.title}
                            </div>
                        </div>
                        <div className='article-content-area'>

                            <div className='content-area'>
                                <Editor blockRendererFn={this.mediaBlockRenderer} editorState={content} readOnly={true} />
                            </div>
                        </div>
                    </div>
                    <div className='related-articles' style={{ width: '25%' }}>
                        <div style={{ width: '100%', textAlign: 'center' }}>Related articles</div>
                        {
                            this.state.similar.map(item => {
                                if (item.id !== this.state.article.id)
                                    return <div className='related-container'>
                                        <div className='left-hover-indicator'></div>
                                        <div className='image-thumbnail'>
                                            <Image src='/assets/article-banner.png' />
                                        </div>
                                        <div style={{ paddingLeft: '10px', cursor: 'pointer' }}>
                                            <strong onClick={() => this.props.history.push('/article/' + item.id)}>
                                                {item.title}
                                            </strong>
                                        </div>
                                    </div>
                            })
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(connect(null, mapDispatchToProps)(Article));