import React, { Component } from "react";
import { connect } from "react-redux";
import { Image } from 'react-bootstrap';
import { withRouter } from "react-router-dom";
import Icons from 'react-icons-kit';
import { fileTextO } from 'react-icons-kit/fa/fileTextO';
import { bindActionCreators } from 'redux';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { TOGGLE_LOADER } from '../../redux/actions/actionsTypes';
import agent from "../../services/agent";
import './Home.css';

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    toggleLoader: value => dispatch({ type: TOGGLE_LOADER, value }),
}, dispatch)

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            articles: [],
            dataToRender: [],
        }
    }

    componentDidMount() {
        this.props.toggleLoader(true);
        agent.Blog.listArticle()
            .then(res => {
                this.props.toggleLoader(false);
                try {
                    if (res.status === 200) {
                        this.setState({
                            articles: res.data.result,
                            dataToRender: res.data.result,
                        })
                    } else {
                        this.setState({
                            articles: [],
                            dataToRender: [],
                        })
                    }
                } catch (e) {
                    toast.dark('Something went wrong, please try again.')
                }
            })
    }

    showArticle = id => {
        this.props.history.push('/article/' + id);
    }

    render() {
        let articles = [];
        this.state.dataToRender.map((item, index) => {
            articles.push(
                <div className='article-container' key={index}>
                    <div className='left-hover-indicator'></div>
                    <div className='image-thumbnail'>
                        <Image src='/assets/article-banner.png' />
                    </div>
                    <div className='content-area'>
                        <strong onClick={() => this.showArticle(item.id)}>{item.title}</strong>
                        <small>{item.topic_name}
                        </small>
                        <div className='status-area'>
                            <Icons icon={fileTextO} size={14} />
                        </div>
                    </div>
                </div>
            )
        })
        return (
            <div className="full-space">
                <div className='article-list-container'>
                    {articles}
                </div>
            </div>
        );
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home))