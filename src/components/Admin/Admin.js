import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Switch } from "react-router-dom";
import { toast } from 'react-toastify';

import PrivateRoute from '../../utility/PrivateRoute';
import agent from '../../services/agent';
import AdminHome from '../AdminHome/AdminHome';
import Topics from '../Topics/Topics';
import CreateArticle from '../CreateArticle/CreateArticle';
import './Admin.css';

const mapStateToProps = state => {
    return {
        loader: state.common.loader,
        isAuthenticated: state.auth.isAuthenticated,
        isAdmin: state.auth.userInfo.isAdmin,
    }
}

class Admin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            topics: []
        }
    }

    listTopics = () => {
        agent.Blog.listTopics()
        .then(res => {
            try {
                if (res.status === 200) {
                    this.setState({
                        topics: res.data.result,
                    });
                } else {
                    toast.dark('Something went wrong. Please try again.');
                }
            } catch (e) {
                toast.dark('Could not fetch your data. Contact your administrator');
            }
        });
    } 

    componentDidMount() {
        this.listTopics();
    }

    addTopic = (id, topic) => {
        this.listTopics();
    }

    render() {
        return (
            <div className='full-space'>
                <Switch>
                    <PrivateRoute
                        exact
                        path='/admin/home'
                        isAuthenticated={this.props.isAuthenticated}
                        isAdmin={this.props.isAdmin}
                        component={AdminHome}
                    />
                    <PrivateRoute
                        exact
                        path='/admin/manage-admin'
                        isAuthenticated={this.props.isAuthenticated}
                        isAdmin={this.props.isAdmin}
                        component={AdminHome}
                    />
                    <PrivateRoute
                        exact
                        path='/admin/manage-topics'
                        isAuthenticated={this.props.isAuthenticated}
                        isAdmin={this.props.isAdmin}
                        component={() => <Topics topics={this.state.topics} append={this.addTopic} />}
                    />
                    <PrivateRoute
                        exact
                        path='/admin/create-article'
                        isAuthenticated={this.props.isAuthenticated}
                        isAdmin={this.props.isAdmin}
                        component={() => <CreateArticle topics={this.state.topics} />}
                    />
                </Switch>
            </div>
        );
    }
}

export default withRouter(connect(mapStateToProps, null)(Admin));
