import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import { Button } from 'reactstrap';
import { Image } from 'react-bootstrap';
import { toast } from 'react-toastify';

import './Topics.css';
import agent from '../../services/agent';

class Topics extends Component {
    constructor(props) {
        super(props)
        this.state = {
            topics: this.props.topics,
            topic: {
                id: -1,
                name: '',
                image: '',
            },
        }
    }

    inputHandler = input => event => {
        this.setState({
            topic: {
                ...this.state.topic,
                [input]: event.target.value
            }
        });
    }

    save = () => {
        let formData = new FormData();
        formData.set('image', this.state.topic.image);
        formData.set('name', this.state.topic.name);
        agent.Blog.createTopic(formData)
            .then(res => {
                try {
                    if (res.status === 201) {
                        this.props.append(res.id, this.state.topic);
                        this.setState({
                            topic: {
                                id: -1,
                                name: '',
                                image: '',
                            },
                        })
                    }
                } catch (e) {
                    toast.dark('Data could not be saved')
                }
            })
    }

    uploadImage = event => {
        this.setState({
            topic: {
                ...this.state.topic,
                image: event.target.files[0]
            },
        })
    }

    render() {
        let { topics, topic } = this.state;
        let topicJsx = [];
        topics.map((item, index) => {
            topicJsx.push(
                <div className='topic-display-row' key={index}>
                    <div className='topic-display-code'>
                        <Image className='topic-display-image' src={`data:image/jpeg;base64,${item.image}`} />
                    </div>
                    <div className='topic-display-code'>
                        {item.name}
                    </div>
                </div>
            );
        });

        return (
            <div className="topic-main">
                <div className='back-button-area'>
                    <Button className='article-back-button' onClick={() => this.props.history.push('/admin/home')}>
                        {'<'} Back
                    </Button>
                </div>
                <div className='topic-page-content'>
                    <div className='topic-data-content '>
                        <div className='list-topics'>
                            <div className="list-topic-title">
                                Your Topics
                            </div>
                            <div className='topic-display-title' key={'title'}>
                                <div className='topic-display-code'>
                                    {'Image'}
                                </div>
                                <div className='topic-display-code'>
                                    {'Name'}
                                </div>
                            </div>
                            <div className='list-topic-content'>
                                {topicJsx}
                            </div>
                        </div>
                        <div className='modify-topics'>
                            <div className="list-topic-title">
                                Modify
                            </div>
                            <label className='modify-labels'>Name</label>
                            <div className='input-container-topic'>
                                <input
                                    placeholder="Name"
                                    className='modify-input'
                                    value={topic.name}
                                    onChange={this.inputHandler('name')}
                                />
                            </div>
                            <div className='input-container-topic'>
                                <input
                                    className='modify-input'
                                    type="file"
                                    accept="image/*"
                                    ref="uploadImage"
                                    onChange={this.uploadImage}
                                />
                            </div>
                            <div className='modify-topic-button-container'>
                                <Button
                                    disabled={topic.name.length === 0 || topic.image === ''}
                                    className='modify-topic-button'
                                    onClick={this.save}
                                > Save
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default withRouter(Topics);