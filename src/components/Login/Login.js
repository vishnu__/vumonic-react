import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter, Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { STRING } from '../../utility/utilityFunction';
import { UPDATE_ISAUTHENTICATED, UPDATE_USER_DATA, TOGGLE_LOADER, UPDATE_TOKEN } from '../../redux/actions/actionsTypes';
import agent from '../../services/agent';
import './Login.css';

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    toggleLoader: value => dispatch({ type: TOGGLE_LOADER, value }),
    updateToken: value => dispatch({ type: UPDATE_TOKEN, value }),
    updateUserInfo: value => dispatch({ type: UPDATE_USER_DATA, value }),
    onAuthentication: value => dispatch({ type: UPDATE_ISAUTHENTICATED, value }),
}, dispatch)

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            activeEmailField: false,
            activePasswordField: false,
            disabledButton: true,
        }
    }

    activateField = input => {
        this.setState({
            [input]: true,
        })
    }

    deactivateField = (input, event) => {
        if (event.target.value === '') {
            this.setState({
                [input]: false,
            })
        }
    }

    inputHandler = (input, event) => {
        let value = event.target.value;
        let inputValue = this.state[input]
        if (input === 'phone') {
            if (value === '' || /^[0-9\b]+$/.test(value)) {
                inputValue = value;
            }
        } else inputValue = value
        this.setState({
            [input]: inputValue,
        }, () => {
            this.activateButton();
        })
        event.preventDefault();
    }

    activateButton = () => {
        if (STRING.validateEmail(this.state.email) && this.state.password.length >= 4) {
            this.setState({ disabledButton: false })
        }
        else this.setState({ disabledButton: true })
    }

    enter = event => {
        if(event.key === 'Enter' && !this.state.disabledButton) {
            this.login();
        }
    }

    login = () => {
        this.props.toggleLoader(true);
        agent.Auth.login({ 
            email: this.state.email,
            password: this.state.password
        }).then(res => {
            this.props.toggleLoader(false);
            console.log(res, res.status)
            try {
                if (res.status === 200) {
                    let user_info = {
                        ...res.data.user_info,
                        isAdmin: res.data.is_admin,
                    }
                    this.props.updateToken(res.data.token);
                    this.props.updateUserInfo(user_info);
                    this.props.onAuthentication(true);
                    localStorage.setItem('token', res.data.token);
                    localStorage.setItem('lastRefresh', Date.now());
                    localStorage.setItem('user_info', JSON.stringify(user_info));
                    localStorage.setItem('isAuthenticated', true);
                    this.props.history.push('/home')
                } else {
                    toast.dark("Wrong username or password");
                }
            } catch(e) {
                toast.dark("Something went wrong, please contact admin.");
            }
        })
    }

    render() {
        return (
            <div className="auth-wrapper full-space">
                <div className="auth-inner">
                    <div className='auth-header'>
                        Sign In
                    </div>
                    <form className='paddingy-5'>
                        <div className='login-input-area'>
                            <label
                                onClick={() => this.refs.emailInput.focus()}
                                className={'login-label ' + (this.state.activeEmailField ? 'active' : '')}
                            >
                                Email
                            </label>
                            <input
                                type='text'
                                ref='emailInput'
                                className='login-floating-label'
                                value={this.state.email}
                                onChange={event => this.inputHandler('email', event)}
                                onFocus={() => this.activateField('activeEmailField')}
                                onBlur={event => this.deactivateField('activeEmailField', event)}
                            />
                        </div >
                        <div className='login-input-area'>
                            <label
                                onClick={() => this.refs.passwordInput.focus()}
                                className={'login-label ' + (this.state.activePasswordField ? 'active' : '')}
                            >
                                Password
                            </label>
                            <input
                                type='password'
                                ref='passwordInput'
                                className='login-floating-label'
                                value={this.state.password}
                                onKeyDown={this.enter}
                                onChange={event => this.inputHandler('password', event)}
                                onFocus={() => this.activateField('activePasswordField')}
                                onBlur={event => this.deactivateField('activePasswordField', event)}
                            />
                        </div >
                    </form>
                    <div>
                        <Button
                            onClick={this.login}
                            // type="submit"
                            className="submit-button"
                            disabled={this.state.disabledButton}
                        >
                            Submit
                        </Button>
                    </div>
                </div>
                <div className="auth-inner">
                    <div className='switch-pages-bottom '>
                        Not a member? <Link to={"/sign-up"}>Sign Up</Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login))