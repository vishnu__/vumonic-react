import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { Button } from 'reactstrap';

import { STRING } from '../../utility/utilityFunction';
import agent from '../../services/agent';
import { TOGGLE_LOADER } from '../../redux/actions/actionsTypes';
import { toast } from 'react-toastify';
import './SignUp.css'

const mapDispatchToProps = dispatch => bindActionCreators({
    toggleLoader: value => dispatch({ type: TOGGLE_LOADER, value }),
}, dispatch)

class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            age: '',
            gender: '',
            password: '',
            confirm: '',
            activeNameField: false,
            activeAgeField: false,
            activeGenderField: false,
            activeEmailField: false,
            activePasswordField: false,
            activeConfirmField: false,
            disabledButton: true,
        }
    }

    activateField = input => {
        this.setState({
            [input]: true,
        })
    }

    deactivateField = (input, event) => {
        if (event.target.value === '') {
            this.setState({
                [input]: false,
            })
        }
    }

    inputHandler = (input, event) => {
        let value = event.target.value;
        let inputValue = this.state[input]
        if (input === 'age') {
            if (value === '' || /^[0-9\b]+$/.test(value)) {
                inputValue = value;
            }
        } else inputValue = value
        this.setState({
            [input]: inputValue,
        }, () => {
            this.activateButton();
        })
        event.preventDefault();
    }

    handleChange = input => event => {
        let errors = { ...this.state.errorMessages };
        errors[input] = '';
        this.setState({
            [input]: event.target.value,
            errorMessages: errors,
        })
    }

    enter = event => {
        if(event.key === 'Enter' && !this.state.disabledButton) {
            this.signup();
        }
    }

    signup = (e) => {
        e.preventDefault();
        this.props.toggleLoader(true);
        agent.Auth.signup({
            email: this.state.email,
            password: this.state.password,
            profile: {
                name: this.state.name,
                age: parseInt(this.state.age),
                gender: this.state.gender,
            }
        }).then(res => {
            this.props.toggleLoader(false);
            try {
                if(res.status === 201) {
                    toast.dark('User registered successfully').
                    this.props.history.push('/sign-in')
                }
            } catch (e) {
                toast.dark('Something went wrong, please try again.')
            }
        })
    }

    activateButton = () => {
        if (STRING.validateEmail(this.state.email) && this.state.age.length <= 3 && this.state.name.length > 2
            && this.state.confirm === this.state.password && this.state.password.length >= 4 && this.state.gender.length > 0 ) {
            this.setState({ disabledButton: false })
        }
        else this.setState({ disabledButton: true })
    }

    render() {
        return (
            <div className="auth-wrapper full-space">
                <div className="auth-inner">
                    <div className='auth-header'>
                        Sign Up
                    </div>
                    <form className='paddingy-5'>
                        <div className='login-input-area'>
                            <label
                                onClick={() => this.refs.nameInput.focus()}
                                className={'login-label ' + (this.state.activeNameField ? 'active' : '')}
                            >
                                Name
                            </label>
                            <input
                                type='text'
                                ref='nameInput'
                                className='login-floating-label'
                                onKeyDown={this.enter}
                                value={this.state.name}
                                onChange={event => this.inputHandler('name', event)}
                                onFocus={() => this.activateField('activeNameField')}
                                onBlur={event => this.deactivateField('activeNameField', event)}
                            />
                        </div>
                        <div className='flex-row'>
                            <div className='login-input-area'>
                                <label
                                    onClick={() => this.refs.ageInput.focus()}
                                    className={'login-label ' + (this.state.activeAgeField ? 'active' : '')}
                                >
                                    Age
                                </label>
                                <input
                                    type='text'
                                    ref='ageInput'
                                    className='login-floating-label'
                                    onKeyDown={this.enter}
                                    value={this.state.age}
                                    onChange={event => this.inputHandler('age', event)}
                                    onFocus={() => this.activateField('activeAgeField')}
                                    onBlur={event => this.deactivateField('activeAgeField', event)}
                                />
                            </div>
                            <div className='login-input-area'>
                                <label
                                    onClick={() => this.refs.genderInput.focus()}
                                    className={'login-label ' + (this.state.activeGenderField ? 'active' : '')}
                                >
                                    Gender
                                </label>
                                <input
                                    type='text'
                                    ref='genderInput'
                                    className='login-floating-label'
                                    onKeyDown={this.enter}
                                    value={this.state.gender}
                                    onChange={event => this.inputHandler('gender', event)}
                                    onFocus={() => this.activateField('activeGenderField')}
                                    onBlur={event => this.deactivateField('activeGenderField', event)}
                                />
                            </div>
                        </div>
                        <div className='login-input-area'>
                            <label
                                onClick={() => this.refs.emailInput.focus()}
                                className={'login-label ' + (this.state.activeEmailField ? 'active' : '')}
                            >
                                Email
                            </label>
                            <input
                                type='text'
                                ref='emailInput'
                                className='login-floating-label'
                                onKeyDown={this.enter}
                                value={this.state.email}
                                onChange={event => this.inputHandler('email', event)}
                                onFocus={() => this.activateField('activeEmailField')}
                                onBlur={event => this.deactivateField('activeEmailField', event)}
                            />
                        </div >
                        <div className='login-input-area'>
                            <label
                                onClick={() => this.refs.passwordInput.focus()}
                                className={'login-label ' + (this.state.activePasswordField ? 'active' : '')}
                            >
                                Password
                            </label>
                            <input
                                type='password'
                                ref='passwordInput'
                                className='login-floating-label'
                                onKeyDown={this.enter}
                                value={this.state.password}
                                onChange={event => this.inputHandler('password', event)}
                                onFocus={() => this.activateField('activePasswordField')}
                                onBlur={event => this.deactivateField('activePasswordField', event)}
                            />
                        </div >
                        <div className='login-input-area'>
                            <label
                                onClick={() => this.refs.confirmInput.focus()}
                                className={'login-label ' + (this.state.activeConfirmField ? 'active' : '')}
                            >
                                Confirm Password
                            </label>
                            <input
                                type='text'
                                ref='confirmInput'
                                className='login-floating-label'
                                onKeyDown={this.enter}
                                value={this.state.confirm}
                                onChange={event => this.inputHandler('confirm', event)}
                                onFocus={() => this.activateField('activeConfirmField')}
                                onBlur={event => this.deactivateField('activeConfirmField', event)}
                            />
                        </div >
                    </form>
                    <div>
                        <Button
                            onClick={this.signup}
                            // type="submit"
                            className="submit-button"
                            disabled={this.state.disabledButton}
                        >
                            Submit
                        </Button>
                    </div>
                </div>
                <div className="auth-inner">
                    <div className='switch-pages-bottom '>
                        Already a member? <Link to={"/sign-in"}>Sign In</Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(connect(null, mapDispatchToProps)(SignUp));