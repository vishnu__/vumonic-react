import React, { Component } from 'react';
import { thinLeft } from 'react-icons-kit/entypo/thinLeft';
import { Button } from 'react-bootstrap';
import Icon from 'react-icons-kit';
import { bin } from 'react-icons-kit/ikons/bin';
import { EditorState, convertToRaw, convertFromRaw } from 'draft-js';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'draft-js/dist/Draft.css';

import agent from '../../services/agent';
import RichTextEditor from '../RichTextEditor/RichTextEditor';
import "./CreateArticle.css";


class CreateArticle extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            title: '',
            tags: '',
            image: '',
            titleCount: 0,
            body: EditorState.createEmpty(),
            topic: {},
            is_featured: false,
            topicsOpen: false
        }
    }

    // updatePicture = (picture, thumbnail) => event => {

    //     this.setState({
    //         [picture]: event.target.files[0],
    //     }, () => {
    //         console.log(this.state.coverPicture)
    //     })
    //     let reader = new FileReader();
    //     reader.onloadend = () => {
    //         this.setState({
    //             [thumbnail]: reader.result,
    //         });
    //     }
    //     reader.readAsDataURL(event.target.files[0])
    // }

    toggleDropdown = input => {
        this.setState({
            [input]: !this.state[input],
        })
    }

    handleDropDown = index => {
        this.setState({
            topic: this.props.topics[index],
        })
    }

    updateState = input => state => {
        this.setState({
            [input]: state,
        })
    }


    uploadImage = event => {
        this.setState({
            image: event.target.files[0]
        })
    }

    inputHandler = input => (event, length) => {
        let value = event.target.value;
        if (event.target.value.length > length)
            value = value.substring(0, length);
        this.setState({
            [input]: value,
        }, () => {
            let count = input + 'Count';
            this.setState({
                [count]: this.state[input].length,
            })
        })
    }

    toggleFeatured = () => {
        this.setState({
            is_featured: !this.state.is_featured,
        })
    }

    savePost = () => {
        if (this.state.titleCount === 0) {
            toast.dark('Title is required');
            return;
        }
        if (this.state.topic.name === undefined) {
            toast.dark('Select a topic');
            return;
        }
        if (this.state.image === '') {
            toast.dark('Upload an image');
            return;
        }
        let content = this.state.body.getCurrentContent();
        let isEditorEmpty = !content.hasText();
        let currentPlainText = content.getPlainText();
        let lengthOfTrimmedContent = currentPlainText.trim().length;
        console.log('editor containe some text (not only spaces) => ', !!(!isEditorEmpty && lengthOfTrimmedContent))
        if (!(!isEditorEmpty && lengthOfTrimmedContent)) {
            toast.warn('Editor must contain text');
            return;
        }
        let formData = new FormData();
        let tags = this.state.tags.split(",").map(item => item.trim());
        formData.set('image', this.state.image);
        formData.set('title', this.state.title);
        formData.set('topic', this.state.topic.id);
        formData.set('content', JSON.stringify(convertToRaw(this.state.body.getCurrentContent())));
        formData.set('is_featured', this.state.is_featured);
        formData.set('tags', JSON.stringify(tags));
        agent.Blog.createArticle(formData)
            .then(res => {
                try {
                    if (res.status === 201) {
                        toast.info('Article created')
                        this.props.history.push('/admin/home')
                    } else {
                        toast.dark('Something went wrong please try again.')
                    }
                } catch (e) {
                    toast.dark('Something went wrong please try again.')
                }
            })
    }

    render() {
        console.log(this.props.topics)
        return (
            <div className="post-creation-page-main">
                <div className='back-button-area'>
                    <Button className='article-back-button' onClick={() => this.props.history.push('/admin/home')}>
                        {'<'} Back
                    </Button>
                </div>
                <div className="post-creation-page-content">
                    <div className='label-content'>
                        <div>
                            <strong>Title</strong>
                        </div>
                        <div className='input-container'>
                            <input
                                className='input-area'
                                value={this.state.title}
                                onChange={event => this.inputHandler('title')(event, 150)}
                                placeholder={'Enter title here'}
                            />
                            <div className='input-count'>
                                {this.state.titleCount}/150
                                </div>
                        </div>
                    </div>
                    <div className='label-content'>
                        <div>
                            <strong>Tags</strong>
                        </div>
                        <div className='input-container'>
                            <input
                                className='input-area'
                                value={this.state.tags}
                                onChange={event => this.inputHandler('tags')(event, 150)}
                                placeholder={'Enter tags separated by comma (new,recommended)'}
                            />
                        </div>
                    </div>
                    <div className='miscellaneous'>
                        <div>
                            <input type='checkbox' onChange={this.toggleFeatured} checked={this.state.is_featured} />
                            <text onClick={this.toggleFeatured}>Featured</text>
                        </div>
                        <input
                            className='modify-input'
                            type="file"
                            accept="image/*"
                            ref="uploadImage"
                            onChange={this.uploadImage}
                        />
                        <Dropdown
                            isOpen={this.state.topicsOpen}
                            toggle={() => this.toggleDropdown('topicsOpen')}
                            className="topic-select-dropdown"
                        >
                            <DropdownToggle>
                                {this.state.topic.name === undefined ? 'Select Topic' : this.state.topic.name}
                            </DropdownToggle>
                            <DropdownMenu>
                                {this.props.topics.map((topic, index) => {
                                    return <DropdownItem
                                        key={index}
                                        eventKey={index}
                                        onClick={() => this.handleDropDown(index)}
                                    >
                                        {topic.name}
                                    </DropdownItem>
                                })}
                            </DropdownMenu>
                        </Dropdown>
                    </div>
                    <div className='image-upload-header'>
                        <strong>Body</strong>
                    </div>
                    <RichTextEditor image editorState={this.state.body} updateState={this.updateState('body')} />
                </div>

                <div className='RichEditor-button-area'>
                    <div
                        className='RichEditor-save-button'
                        onClick={this.savePost}
                    >
                        Save
                    </div>
                </div>
            </div>
        )
    }
}
export default withRouter(CreateArticle);