import React from 'react';
import { withRouter } from "react-router-dom";

import './AdminHome.css';

const AdminHome = (props) => {

    return (
        <div className="full-height flex-wrap">
            <div className='admin-actions' >
                Manage Admin
            </div>
            <div className='admin-actions' onClick={() => props.history.push('/admin/manage-topics')}>
                Manage Topics
            </div>
            <div className='admin-actions' onClick={() => props.history.push('/admin/create-article')}>
                Create Article
            </div>
        </div>
    );
}

export default withRouter(AdminHome);
